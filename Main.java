package com.company;

import java.io.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.io.IOException;
import java.util.Calendar;
import com.company.sewa;
import com.company.Kategori;
public class Main{
    public static void main(String[] args) {
        BufferedReader isr = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("=================================================");
            System.out.println("        RENTAL VCD SEMUA ADA");
            System.out.println("            TERLENGKAP");
            System.out.println("       Cilacap, Jl.Jeruk N0.17");
            System.out.println("=================================================");
            int pil;
            do {
                System.out.println("1. Peminjaman");
                System.out.println("2. Pengembalian");
                System.out.print("Pilihan Anda  : ");
                pil = Integer.parseInt(isr.readLine());
                switch (pil) {
                    case 1:
                        System.out.println("-------------------------------------------------");
                        System.out.println("\t\t\t\tPEMINJAMAN");
                        System.out.println("-------------------------------------------------");
                        System.out.print("Nama Penyewa\t\t: ");
                        String nama = isr.readLine();
                        System.out.print("No. HP\t\t\t\t: ");
                        String nohp = isr.readLine();
                        System.out.print("Alamat\t\t\t\t: ");
                        String alamat = isr.readLine();
                        System.out.print("Kode VCD \t\t\t: ");
                        String kategori = isr.readLine();
                        System.out.print("Judul VCD\t\t\t: ");
                        String judul = isr.readLine();
                        System.out.print("Jaminan\t\t\t\t: ");
                        String jaminan = isr.readLine();
                        System.out.println("-------------------------------------------------");
                        System.out.println("        RENTAL VCD SEMUA ADA");
                        System.out.println("            TERLENGKAP");
                        System.out.println("       Cilacap, Jl.Jeruk N0.17");
                        System.out.println("-------------------------------------------------");
                        System.out.println("=================KWITANSI========================");
                        sewa n = new sewa();
                        n.setNama(nama);
                        n.setNohp(nohp);
                        n.setAlamat(alamat);
                        n.setJudul(judul);
                        n.setJaminan(jaminan);

                        Kategori k = new Kategori();
                        k.setKategori(kategori);
                        System.out.println("Nama Penyewa\t\t: " + n.getNama());
                        System.out.println("No.Hp\t\t\t\t: " + n.getNohp());
                        System.out.println("Alamat\t\t\t\t: " + n.getAlamat());
                        System.out.println("Judul VCD\t\t\t: " + n.getJudul());
                        System.out.println("Jaminan\t\t\t\t: " + n.getJaminan());
                        System.out.println("Kode VCD\t\t\t: " + n.getKategori());
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy1");
                        Calendar tgl = Calendar.getInstance();
                        System.out.println("Tanggal Pinjam\t\t: " + tgl.getTime());
                        tgl.add(Calendar.DATE, 4);
                        System.out.println("BATAS PENGEMBALIAN\t: " + sdf.format(tgl.getTime()));
                        System.out.println("Denda telat /hari \t: Rp.3000");
                        System.out.println();
                        System.out.println("=================JANGAN TELAT YA=================");
                        break;
                    case 2:
                        System.out.println("-------------------------------------------------");
                        System.out.println("\t\t\t\tPENGEMBALIAN ");
                        System.out.println("-------------------------------------------------");
                        System.out.print("Nama Penyewa\t\t: ");
                        String namap = isr.readLine();
                        System.out.print("No. HP\t\t\t\t: ");
                        String nohpp = isr.readLine();
                        System.out.print("Alamat\t\t\t\t: ");
                        String alamatp = isr.readLine();
                        System.out.print("Kategori VCD\t\t: ");
                        String kategorip = isr.readLine();
                        System.out.print("Judul VCD\t\t\t: ");
                        String judulp = isr.readLine();
                        System.out.print("Jaminan\t\t\t\t: ");
                        String jaminanp = isr.readLine();
                        System.out.print("Tanggal Peminjaman\t: ");
                        int tglpnjm = Integer.parseInt(isr.readLine());
                        System.out.print("Bulan dan Tahun\t: ");
                        String bln = isr.readLine();
                        System.out.print("Tanggal Pengembalian\t: ");
                        int tglkmbl = Integer.parseInt(isr.readLine());
                        System.out.println("Bulan dan Tahun\t: ");
                        String blnk = isr.readLine();
                        System.out.println("-------------------------------------------------");
                        System.out.println("        RENTAL VCD SEMUA ADA");
                        System.out.println("            TERLENGKAP");
                        System.out.println("       Cilacap, Jl.Jeruk N0.17");
                        System.out.println("-------------------------------------------------");
                        System.out.println("=================KWITANSI========================");
                        sewa m = new sewa();
                        m.setNama(namap);
                        m.setNohp(nohpp);
                        m.setAlamat(alamatp);
                        m.setJudul(judulp);
                        m.setJaminan(jaminanp);

                        Kategori t = new Kategori();
                        t.setKategori(kategorip);
                        System.out.println("Nama Penyewa\t\t: "+m.getNama());
                        System.out.println("No.HP\t\t\t: " + m.getNohp());
                        System.out.println("Alamat\t\t\t: " + m.getAlamat());
                        System.out.println("Judul VCD\t\t: " + m.getJudul());
                        System.out.println("Jaminan\t\t: " + m.getJaminan());
                        System.out.println("Kode VCD\t\t: " + m.getKategori());
                        System.out.println("Tanggal Peminjaman\t: " + tglpnjm + " " + bln);
                        System.out.println("Tanggal Pengembalian\t: " + tglkmbl + " " + blnk);
                        int batas = 4;
                        int bataskmbl = tglpnjm + batas;
                        System.out.println("BATAS PENGEMBALIAN\t: Tanggal " + bataskmbl + " " + bln);
                        int telat = tglkmbl - bataskmbl;
                        int denda = telat * 3000;
                        System.out.println("Denda telat /hari \t: " + telat + " x Rp.3000 = " + denda);
                        System.out.println();
                        System.out.println("==================JANGAN TELAT YA================");
                        break;
                }
                System.out.println();
                System.out.println("0. Menu Utama");
                System.out.println("9. Tutup");
                System.out.print("Pilihan Anda[0/9] :");
                pil = Integer.parseInt(isr.readLine());
            } while (pil != 9);
        } catch (IOException e) {
            System.out.println("Erorr!!!" + e.getMessage());
        }
    }
}



